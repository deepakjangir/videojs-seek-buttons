/*! @name videojs-seek-buttons @version 1.5.0 @license Apache-2.0 */
(function (QUnit, sinon, videojs) {
	'use strict';

	QUnit = QUnit && QUnit.hasOwnProperty('default') ? QUnit['default'] : QUnit;
	sinon = sinon && sinon.hasOwnProperty('default') ? sinon['default'] : sinon;
	videojs = videojs && videojs.hasOwnProperty('default') ? videojs['default'] : videojs;

	var commonjsGlobal = typeof window !== 'undefined' ? window : typeof global !== 'undefined' ? global : typeof self !== 'undefined' ? self : {};

	var minDoc = {};

	var topLevel = typeof commonjsGlobal !== 'undefined' ? commonjsGlobal :
	    typeof window !== 'undefined' ? window : {};


	var doccy;

	if (typeof document !== 'undefined') {
	    doccy = document;
	} else {
	    doccy = topLevel['__GLOBAL_DOCUMENT_CACHE@4'];

	    if (!doccy) {
	        doccy = topLevel['__GLOBAL_DOCUMENT_CACHE@4'] = minDoc;
	    }
	}

	var document_1 = doccy;

	function _inheritsLoose(subClass, superClass) {
	  subClass.prototype = Object.create(superClass.prototype);
	  subClass.prototype.constructor = subClass;
	  subClass.__proto__ = superClass;
	}

	var version = "1.5.0";

	var cov_g043db1gx = function () {
	  var path = '/Users/bclifford/Code/videojs-seek-buttons/src/plugin.js',
	      hash = '8182d1c091ae93baab981929c1b066d2f5af20e8',
	      Function = function () {}.constructor,
	      global = new Function('return this')(),
	      gcv = '__coverage__',
	      coverageData = {
	    path: '/Users/bclifford/Code/videojs-seek-buttons/src/plugin.js',
	    statementMap: {
	      '0': {
	        start: {
	          line: 4,
	          column: 15
	        },
	        end: {
	          line: 4,
	          column: 45
	        }
	      },
	      '1': {
	        start: {
	          line: 7,
	          column: 17
	        },
	        end: {
	          line: 7,
	          column: 19
	        }
	      },
	      '2': {
	        start: {
	          line: 10,
	          column: 23
	        },
	        end: {
	          line: 10,
	          column: 63
	        }
	      },
	      '3': {
	        start: {
	          line: 35,
	          column: 22
	        },
	        end: {
	          line: 61,
	          column: 1
	        }
	      },
	      '4': {
	        start: {
	          line: 37,
	          column: 2
	        },
	        end: {
	          line: 37,
	          column: 38
	        }
	      },
	      '5': {
	        start: {
	          line: 39,
	          column: 2
	        },
	        end: {
	          line: 48,
	          column: 3
	        }
	      },
	      '6': {
	        start: {
	          line: 40,
	          column: 4
	        },
	        end: {
	          line: 43,
	          column: 7
	        }
	      },
	      '7': {
	        start: {
	          line: 44,
	          column: 4
	        },
	        end: {
	          line: 47,
	          column: 6
	        }
	      },
	      '8': {
	        start: {
	          line: 50,
	          column: 2
	        },
	        end: {
	          line: 59,
	          column: 3
	        }
	      },
	      '9': {
	        start: {
	          line: 51,
	          column: 4
	        },
	        end: {
	          line: 54,
	          column: 7
	        }
	      },
	      '10': {
	        start: {
	          line: 55,
	          column: 4
	        },
	        end: {
	          line: 58,
	          column: 6
	        }
	      },
	      '11': {
	        start: {
	          line: 75,
	          column: 20
	        },
	        end: {
	          line: 80,
	          column: 1
	        }
	      },
	      '12': {
	        start: {
	          line: 77,
	          column: 2
	        },
	        end: {
	          line: 79,
	          column: 5
	        }
	      },
	      '13': {
	        start: {
	          line: 78,
	          column: 4
	        },
	        end: {
	          line: 78,
	          column: 65
	        }
	      },
	      '14': {
	        start: {
	          line: 83,
	          column: 0
	        },
	        end: {
	          line: 83,
	          column: 30
	        }
	      },
	      '15': {
	        start: {
	          line: 95,
	          column: 4
	        },
	        end: {
	          line: 95,
	          column: 27
	        }
	      },
	      '16': {
	        start: {
	          line: 96,
	          column: 4
	        },
	        end: {
	          line: 102,
	          column: 5
	        }
	      },
	      '17': {
	        start: {
	          line: 97,
	          column: 6
	        },
	        end: {
	          line: 98,
	          column: 56
	        }
	      },
	      '18': {
	        start: {
	          line: 99,
	          column: 11
	        },
	        end: {
	          line: 102,
	          column: 5
	        }
	      },
	      '19': {
	        start: {
	          line: 100,
	          column: 6
	        },
	        end: {
	          line: 101,
	          column: 56
	        }
	      },
	      '20': {
	        start: {
	          line: 113,
	          column: 4
	        },
	        end: {
	          line: 114,
	          column: 63
	        }
	      },
	      '21': {
	        start: {
	          line: 118,
	          column: 16
	        },
	        end: {
	          line: 118,
	          column: 42
	        }
	      },
	      '22': {
	        start: {
	          line: 120,
	          column: 4
	        },
	        end: {
	          line: 124,
	          column: 5
	        }
	      },
	      '23': {
	        start: {
	          line: 121,
	          column: 6
	        },
	        end: {
	          line: 121,
	          column: 60
	        }
	      },
	      '24': {
	        start: {
	          line: 122,
	          column: 11
	        },
	        end: {
	          line: 124,
	          column: 5
	        }
	      },
	      '25': {
	        start: {
	          line: 123,
	          column: 6
	        },
	        end: {
	          line: 123,
	          column: 60
	        }
	      },
	      '26': {
	        start: {
	          line: 128,
	          column: 0
	        },
	        end: {
	          line: 128,
	          column: 52
	        }
	      },
	      '27': {
	        start: {
	          line: 131,
	          column: 0
	        },
	        end: {
	          line: 131,
	          column: 43
	        }
	      }
	    },
	    fnMap: {
	      '0': {
	        name: '(anonymous_0)',
	        decl: {
	          start: {
	            line: 35,
	            column: 22
	          },
	          end: {
	            line: 35,
	            column: 23
	          }
	        },
	        loc: {
	          start: {
	            line: 35,
	            column: 43
	          },
	          end: {
	            line: 61,
	            column: 1
	          }
	        },
	        line: 35
	      },
	      '1': {
	        name: '(anonymous_1)',
	        decl: {
	          start: {
	            line: 75,
	            column: 20
	          },
	          end: {
	            line: 75,
	            column: 21
	          }
	        },
	        loc: {
	          start: {
	            line: 75,
	            column: 38
	          },
	          end: {
	            line: 80,
	            column: 1
	          }
	        },
	        line: 75
	      },
	      '2': {
	        name: '(anonymous_2)',
	        decl: {
	          start: {
	            line: 77,
	            column: 13
	          },
	          end: {
	            line: 77,
	            column: 14
	          }
	        },
	        loc: {
	          start: {
	            line: 77,
	            column: 19
	          },
	          end: {
	            line: 79,
	            column: 3
	          }
	        },
	        line: 77
	      },
	      '3': {
	        name: '(anonymous_3)',
	        decl: {
	          start: {
	            line: 94,
	            column: 2
	          },
	          end: {
	            line: 94,
	            column: 3
	          }
	        },
	        loc: {
	          start: {
	            line: 94,
	            column: 31
	          },
	          end: {
	            line: 103,
	            column: 3
	          }
	        },
	        line: 94
	      },
	      '4': {
	        name: '(anonymous_4)',
	        decl: {
	          start: {
	            line: 105,
	            column: 2
	          },
	          end: {
	            line: 105,
	            column: 3
	          }
	        },
	        loc: {
	          start: {
	            line: 105,
	            column: 18
	          },
	          end: {
	            line: 115,
	            column: 3
	          }
	        },
	        line: 105
	      },
	      '5': {
	        name: '(anonymous_5)',
	        decl: {
	          start: {
	            line: 117,
	            column: 2
	          },
	          end: {
	            line: 117,
	            column: 3
	          }
	        },
	        loc: {
	          start: {
	            line: 117,
	            column: 16
	          },
	          end: {
	            line: 125,
	            column: 3
	          }
	        },
	        line: 117
	      }
	    },
	    branchMap: {
	      '0': {
	        loc: {
	          start: {
	            line: 10,
	            column: 23
	          },
	          end: {
	            line: 10,
	            column: 63
	          }
	        },
	        type: 'binary-expr',
	        locations: [{
	          start: {
	            line: 10,
	            column: 23
	          },
	          end: {
	            line: 10,
	            column: 45
	          }
	        }, {
	          start: {
	            line: 10,
	            column: 49
	          },
	          end: {
	            line: 10,
	            column: 63
	          }
	        }],
	        line: 10
	      },
	      '1': {
	        loc: {
	          start: {
	            line: 39,
	            column: 2
	          },
	          end: {
	            line: 48,
	            column: 3
	          }
	        },
	        type: 'if',
	        locations: [{
	          start: {
	            line: 39,
	            column: 2
	          },
	          end: {
	            line: 48,
	            column: 3
	          }
	        }, {
	          start: {
	            line: 39,
	            column: 2
	          },
	          end: {
	            line: 48,
	            column: 3
	          }
	        }],
	        line: 39
	      },
	      '2': {
	        loc: {
	          start: {
	            line: 39,
	            column: 6
	          },
	          end: {
	            line: 39,
	            column: 44
	          }
	        },
	        type: 'binary-expr',
	        locations: [{
	          start: {
	            line: 39,
	            column: 6
	          },
	          end: {
	            line: 39,
	            column: 21
	          }
	        }, {
	          start: {
	            line: 39,
	            column: 25
	          },
	          end: {
	            line: 39,
	            column: 44
	          }
	        }],
	        line: 39
	      },
	      '3': {
	        loc: {
	          start: {
	            line: 50,
	            column: 2
	          },
	          end: {
	            line: 59,
	            column: 3
	          }
	        },
	        type: 'if',
	        locations: [{
	          start: {
	            line: 50,
	            column: 2
	          },
	          end: {
	            line: 59,
	            column: 3
	          }
	        }, {
	          start: {
	            line: 50,
	            column: 2
	          },
	          end: {
	            line: 59,
	            column: 3
	          }
	        }],
	        line: 50
	      },
	      '4': {
	        loc: {
	          start: {
	            line: 50,
	            column: 6
	          },
	          end: {
	            line: 50,
	            column: 38
	          }
	        },
	        type: 'binary-expr',
	        locations: [{
	          start: {
	            line: 50,
	            column: 6
	          },
	          end: {
	            line: 50,
	            column: 18
	          }
	        }, {
	          start: {
	            line: 50,
	            column: 22
	          },
	          end: {
	            line: 50,
	            column: 38
	          }
	        }],
	        line: 50
	      },
	      '5': {
	        loc: {
	          start: {
	            line: 96,
	            column: 4
	          },
	          end: {
	            line: 102,
	            column: 5
	          }
	        },
	        type: 'if',
	        locations: [{
	          start: {
	            line: 96,
	            column: 4
	          },
	          end: {
	            line: 102,
	            column: 5
	          }
	        }, {
	          start: {
	            line: 96,
	            column: 4
	          },
	          end: {
	            line: 102,
	            column: 5
	          }
	        }],
	        line: 96
	      },
	      '6': {
	        loc: {
	          start: {
	            line: 99,
	            column: 11
	          },
	          end: {
	            line: 102,
	            column: 5
	          }
	        },
	        type: 'if',
	        locations: [{
	          start: {
	            line: 99,
	            column: 11
	          },
	          end: {
	            line: 102,
	            column: 5
	          }
	        }, {
	          start: {
	            line: 99,
	            column: 11
	          },
	          end: {
	            line: 102,
	            column: 5
	          }
	        }],
	        line: 99
	      },
	      '7': {
	        loc: {
	          start: {
	            line: 120,
	            column: 4
	          },
	          end: {
	            line: 124,
	            column: 5
	          }
	        },
	        type: 'if',
	        locations: [{
	          start: {
	            line: 120,
	            column: 4
	          },
	          end: {
	            line: 124,
	            column: 5
	          }
	        }, {
	          start: {
	            line: 120,
	            column: 4
	          },
	          end: {
	            line: 124,
	            column: 5
	          }
	        }],
	        line: 120
	      },
	      '8': {
	        loc: {
	          start: {
	            line: 122,
	            column: 11
	          },
	          end: {
	            line: 124,
	            column: 5
	          }
	        },
	        type: 'if',
	        locations: [{
	          start: {
	            line: 122,
	            column: 11
	          },
	          end: {
	            line: 124,
	            column: 5
	          }
	        }, {
	          start: {
	            line: 122,
	            column: 11
	          },
	          end: {
	            line: 124,
	            column: 5
	          }
	        }],
	        line: 122
	      }
	    },
	    s: {
	      '0': 0,
	      '1': 0,
	      '2': 0,
	      '3': 0,
	      '4': 0,
	      '5': 0,
	      '6': 0,
	      '7': 0,
	      '8': 0,
	      '9': 0,
	      '10': 0,
	      '11': 0,
	      '12': 0,
	      '13': 0,
	      '14': 0,
	      '15': 0,
	      '16': 0,
	      '17': 0,
	      '18': 0,
	      '19': 0,
	      '20': 0,
	      '21': 0,
	      '22': 0,
	      '23': 0,
	      '24': 0,
	      '25': 0,
	      '26': 0,
	      '27': 0
	    },
	    f: {
	      '0': 0,
	      '1': 0,
	      '2': 0,
	      '3': 0,
	      '4': 0,
	      '5': 0
	    },
	    b: {
	      '0': [0, 0],
	      '1': [0, 0],
	      '2': [0, 0],
	      '3': [0, 0],
	      '4': [0, 0],
	      '5': [0, 0],
	      '6': [0, 0],
	      '7': [0, 0],
	      '8': [0, 0]
	    },
	    _coverageSchema: '332fd63041d2c1bcb487cc26dd0d5f7d97098a6c'
	  },
	      coverage = global[gcv] || (global[gcv] = {});

	  if (coverage[path] && coverage[path].hash === hash) {
	    return coverage[path];
	  }

	  coverageData.hash = hash;
	  return coverage[path] = coverageData;
	}();
	var Button = (cov_g043db1gx.s[0]++, videojs.getComponent('Button'));
	var defaults = (cov_g043db1gx.s[1]++, {});
	var registerPlugin = (cov_g043db1gx.s[2]++, (cov_g043db1gx.b[0][0]++, videojs.registerPlugin) || (cov_g043db1gx.b[0][1]++, videojs.plugin));
	cov_g043db1gx.s[3]++;

	var onPlayerReady = function onPlayerReady(player, options) {
	  cov_g043db1gx.f[0]++;
	  cov_g043db1gx.s[4]++;
	  player.addClass('vjs-seek-buttons');
	  cov_g043db1gx.s[5]++;

	  if ((cov_g043db1gx.b[2][0]++, options.forward) && (cov_g043db1gx.b[2][1]++, options.forward > 0)) {
	    cov_g043db1gx.b[1][0]++;
	    cov_g043db1gx.s[6]++;
	    player.controlBar.seekForward = player.controlBar.addChild('seekButton', {
	      direction: 'forward',
	      seconds: options.forward
	    });
	    cov_g043db1gx.s[7]++;
	    player.controlBar.el().insertBefore(player.controlBar.seekForward.el(), player.controlBar.el().firstChild.nextSibling);
	  } else {
	    cov_g043db1gx.b[1][1]++;
	  }

	  cov_g043db1gx.s[8]++;

	  if ((cov_g043db1gx.b[4][0]++, options.back) && (cov_g043db1gx.b[4][1]++, options.back > 0)) {
	    cov_g043db1gx.b[3][0]++;
	    cov_g043db1gx.s[9]++;
	    player.controlBar.seekBack = player.controlBar.addChild('seekButton', {
	      direction: 'back',
	      seconds: options.back
	    });
	    cov_g043db1gx.s[10]++;
	    player.controlBar.el().insertBefore(player.controlBar.seekBack.el(), player.controlBar.el().firstChild.nextSibling);
	  } else {
	    cov_g043db1gx.b[3][1]++;
	  }
	};

	cov_g043db1gx.s[11]++;

	var seekButtons = function seekButtons(options) {
	  var _this = this;

	  cov_g043db1gx.f[1]++;
	  cov_g043db1gx.s[12]++;
	  this.ready(function () {
	    cov_g043db1gx.f[2]++;
	    cov_g043db1gx.s[13]++;
	    onPlayerReady(_this, videojs.mergeOptions(defaults, options));
	  });
	};

	cov_g043db1gx.s[14]++;
	seekButtons.VERSION = version;

	var SeekButton =
	/*#__PURE__*/
	function (_Button) {
	  _inheritsLoose(SeekButton, _Button);

	  function SeekButton(player, options) {
	    var _this2;

	    cov_g043db1gx.f[3]++;
	    cov_g043db1gx.s[15]++;
	    _this2 = _Button.call(this, player, options) || this;
	    cov_g043db1gx.s[16]++;

	    if (_this2.options_.direction === 'forward') {
	      cov_g043db1gx.b[5][0]++;
	      cov_g043db1gx.s[17]++;

	      _this2.controlText(_this2.localize('Seek forward {{seconds}} seconds').replace('{{seconds}}', _this2.options_.seconds));
	    } else {
	      cov_g043db1gx.b[5][1]++;
	      cov_g043db1gx.s[18]++;

	      if (_this2.options_.direction === 'back') {
	        cov_g043db1gx.b[6][0]++;
	        cov_g043db1gx.s[19]++;

	        _this2.controlText(_this2.localize('Seek back {{seconds}} seconds').replace('{{seconds}}', _this2.options_.seconds));
	      } else {
	        cov_g043db1gx.b[6][1]++;
	      }
	    }

	    return _this2;
	  }

	  var _proto = SeekButton.prototype;

	  _proto.buildCSSClass = function buildCSSClass() {
	    cov_g043db1gx.f[4]++;
	    cov_g043db1gx.s[20]++;
	    return "vjs-seek-button skip-" + this.options_.direction + " " + ("skip-" + this.options_.seconds + " " + _Button.prototype.buildCSSClass.call(this));
	  };

	  _proto.handleClick = function handleClick() {
	    cov_g043db1gx.f[5]++;
	    var now = (cov_g043db1gx.s[21]++, this.player_.currentTime());
	    cov_g043db1gx.s[22]++;

	    if (this.options_.direction === 'forward') {
	      cov_g043db1gx.b[7][0]++;
	      cov_g043db1gx.s[23]++;
	      this.player_.currentTime(now + this.options_.seconds);
	    } else {
	      cov_g043db1gx.b[7][1]++;
	      cov_g043db1gx.s[24]++;

	      if (this.options_.direction === 'back') {
	        cov_g043db1gx.b[8][0]++;
	        cov_g043db1gx.s[25]++;
	        this.player_.currentTime(now - this.options_.seconds);
	      } else {
	        cov_g043db1gx.b[8][1]++;
	      }
	    }
	  };

	  return SeekButton;
	}(Button);

	cov_g043db1gx.s[26]++;
	videojs.registerComponent('SeekButton', SeekButton);
	cov_g043db1gx.s[27]++;
	registerPlugin('seekButtons', seekButtons);

	var Player = videojs.getComponent('Player');
	QUnit.test('the environment is sane', function (assert) {
	  assert.strictEqual(typeof Array.isArray, 'function', 'es5 exists');
	  assert.strictEqual(typeof sinon, 'object', 'sinon exists');
	  assert.strictEqual(typeof videojs, 'function', 'videojs exists');
	  assert.strictEqual(typeof seekButtons, 'function', 'plugin is a function');
	});
	QUnit.module('videojs-seek-buttons', {
	  beforeEach: function beforeEach() {
	    // Mock the environment's timers because certain things - particularly
	    // player readiness - are asynchronous in video.js 5. This MUST come
	    // before any player is created; otherwise, timers could get created
	    // with the actual timer methods!
	    this.clock = sinon.useFakeTimers();
	    this.fixture = document_1.getElementById('qunit-fixture');
	    this.video = document_1.createElement('video');
	    this.fixture.appendChild(this.video);
	    this.player = videojs(this.video);
	  },
	  afterEach: function afterEach() {
	    this.player.dispose();
	    this.clock.restore();
	  }
	});
	QUnit.test('registers itself with video.js', function (assert) {
	  assert.expect(2);
	  assert.strictEqual(typeof Player.prototype.seekButtons, 'function', 'videojs-seek-buttons plugin was registered');
	  this.player.seekButtons(); // Tick the clock forward enough to trigger the player to be "ready".

	  this.clock.tick(1);
	  assert.ok(this.player.hasClass('vjs-seek-buttons'), 'the plugin adds a class to the player');
	});
	QUnit.test('adds buttons with classes', function (assert) {
	  this.player.seekButtons({
	    forward: 30,
	    back: 10
	  }); // Tick the clock forward enough to trigger the player to be "ready".

	  this.clock.tick(1);
	  assert.ok(this.player.controlBar.seekBack, 'the plugin adds a back button to the player');
	  assert.ok(this.player.controlBar.seekForward, 'the plugin adds a forward button to the player');
	  assert.ok(this.player.controlBar.seekBack.hasClass('skip-10'), 'the plugin adds a seconds class to the button');
	  assert.ok(this.player.controlBar.seekBack.hasClass('skip-back'), 'the plugin adds a direction class to the button');
	});

}(QUnit, sinon, videojs));
